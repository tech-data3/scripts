# scripts for tech data formation

## create_project.sh

scripts that create an empty python project that allow you to work
as fast as possible.

features : 
- create the dir of the project.
- create the venv.
- create the readme.
- ignore files that you have to ignore.
- allow you to clone a repository in your new project and ignore it 
form the repository of your new project.

```bash
chmod +x create_project.sh

create_project.sh [-g repository] project_name
```

## tmux_session.sh

Scripts that create a tmux session with 4 panes and launch some 
command in it.
Some strange behavior comes out time to time but in general its ok

```bash
chmod +x tmux_session.sh

cd <work_dir> 
<script_path>/tmux_session.sh
```

## tree_status.sh

Scripts that show working dir tree as tree does it which show git status per file
and ignore git ignored file.

TODO: use inotify to redraw not watch.

```bash
chmod +x tree_status.sh

cd <work_dir>
<script_path>/tree_status.sh
```

