#!/bin/bash

# create_project.sh
# This script create empty python project to develop simple personal
# application. 
#                                                       
# Author : Nicolas POPY(popy.nicolas@gmail.com)



WORKSPACE="$HOME/dev"
project_name=
project_to_clone=

help()
{
    echo "usage: create_project.sh [-h|-g git_repo_address] project_name"
    echo
    echo "create a new python project for tech data formation with the"
    echo "option of cloning a git repository in the project dir to begin"
    echo "to work as fast as possible"
    echo
    echo "options:"
    echo "-g git_repo_address    clone a repository in the project dir"
    echo "                       and ignore it in the project repository"
    echo "-h                     display this help"
}

while [ $# -gt 0 ]; do
    case "$1" in 
        -h ) help
            exit 
            ;;
        -g ) shift
            project_to_clone="$1"
            ;;
        * ) if [ $# -eq 1 ]
        then
            project_name="$1"
        else
            help
            exit 1
        fi
    esac
shift
done

if [ -z "$project_name" ]; then
    help
    exit 1
fi

project_dir="$WORKSPACE/$project_name"
if [ -d "$project_dir" ]; then
    echo "$project_name already exists, i dont do anything"
    exit 1
fi

# we create the parent project directory
mkdir "$project_dir" && cd $_

# if there is a project to clone do it and place its name in gitignore
if [ -n "$project_to_clone" ]; then
    git clone "$project_to_clone"
    clone_dir_name="${project_to_clone##*/}"
    echo  "${clone_dir_name%.*}" >> .gitignore
fi

echo "# $project_name" > readme.md
echo "env" >> .gitignore

git init .
git add -A
git commit -m "initial commit"

# create new gitlab repository
project_gitlab="git@gitlab.com:tech-data3/$project_name.git"
git push --set-upstream "$project_gitlab" master

python -m venv ./env

