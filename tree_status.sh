#!/bin/bash


# tree_status.sh
# This script show the working tree with the same output as tree command
# but handle git status to know if file changed
#
# TODO: inotify to refresh better than scan tree everytime
#
# Author : Nicolas POPY(popy.nicolas@gmail.com)

# ansi color
RED='\e[0;31m'
GREEN='\e[0;32m'
B_CYAN='\e[1;35m'
NC='\e[0m' 

ignored_file=($(<$(pwd)/.gitignore))
working_dir=$(pwd)

# Function to check if a file is up to date with the local repository
check_file_status() {

    status=$(git status --porcelain "$1")

    if [ -n "$status" ]; then
        echo -en "${RED}"
    else
        echo -en "${GREEN}"
    fi
}

#Function to recursively display the directory tree with file status
print_tree() {
    local dir="$1"
    local indent="$2"
    local children=($(ls -a "$dir" | grep -Ev '^\$|^\.(\.|git)?$'))
    local counter=0

    # walk through tree
    for index in "${!children[@]}"; do
        local file=${children[index]}
        local name=${file##*/}
        local realpath_relative=$(realpath --relative-to="$working_dir" "$1/$file")
        echo -en "$indent"
        ((counter+=1))
        if [ $counter -ge ${#children[@]} ]; then
            echo -en "\u2514"
        else
            echo -en "\u251C"
        fi
        echo -en "\u2500 "
        if [ -d "$file" ]; then
            if ! [[ " ${ignored_file[@]} " =~ " ${name} " ]]; then
                echo -e "\xF0\x9F\x97\x81  $name"
                print_tree "$1/$file" "$indent\u2502  "
            else
                echo -e "\xF0\x9F\x97\x80  $name "
            fi
        else
            check_file_status $realpath_relative
            echo -e "$name"
        fi
        echo -en "${NC}"
    done
    }

main() {
    echo -e "${B_CYAN}\xF0\x9F\x97\x81 $(pwd)${NC}"
    print_tree "$working_dir" ""
}

main
