#!/bin/bash

# tmux_session.sh
# This script create a new_session with some pane.
# some things go wrong sometimes but in most case it works well
#                                                       
# Author : Nicolas POPY(popy.nicolas@gmail.com)

SESSION_NAME="$(pwd)-session"
COMMAND_SCRIPT="$HOME/scripts/tree_status.sh"
lines=$(tput lines)
cols=$(tput cols)

# Create a new tmux session
tmux new-session -d -s $SESSION_NAME -x $cols -y $lines 

# Split the window vertically to get two panes (top bottom) 
tmux split-window -v -t $SESSION_NAME:0.0 -l $((lines*20/100))

# Split the top pane (left right)
tmux split-window -h -t $SESSION_NAME:0.0 -l $((cols*85/100)) 

# Run commands in specific panes
tmux send-keys -t $SESSION_NAME:0.0 "$COMMAND_SCRIPT" C-m
tmux send-keys -t $SESSION_NAME:0.1 "vim" C-m
tmux send-keys -t $SESSION_NAME:0.3 "source ./env/bin/activate" C-m

# Attach to the created session
tmux attach-session -t $SESSION_NAME

